package com.twuc.webApp.domain.composite.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Profile {

    @Column(nullable = false, length = 128)
    private String city;

    @Column(nullable = false, length = 128)
    private String street;

    public Profile() {
    }

    public Profile(String city, String street) {
        this.city = city;
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }
}

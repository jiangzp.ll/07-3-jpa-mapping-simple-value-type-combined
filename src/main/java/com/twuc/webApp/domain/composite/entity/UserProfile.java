package com.twuc.webApp.domain.composite.entity;

import com.twuc.webApp.domain.composite.entity.Profile;

import javax.persistence.*;

@Entity
public class UserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "city", column = @Column(name = "address_city", nullable = false, length = 128)),
            @AttributeOverride(name = "street", column = @Column(name = "address_street", nullable = false, length = 128))
    })
    private Profile profile;

    public UserProfile() {
    }

    public UserProfile(Profile profile) {
        this.profile = profile;
    }

    public long getId() {
        return id;
    }

    public Profile getProfile() {
        return profile;
    }
}

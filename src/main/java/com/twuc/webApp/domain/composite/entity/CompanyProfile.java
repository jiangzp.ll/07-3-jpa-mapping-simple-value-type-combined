package com.twuc.webApp.domain.composite.entity;

import javax.persistence.*;

@Entity
public class CompanyProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Embedded
    private Profile profile;

    public CompanyProfile() {
    }

    public CompanyProfile(Profile profile) {
        this.profile = profile;
    }

    public long getId() {
        return id;
    }

    public Profile getProfile() {
        return profile;
    }
}

package com.twuc.webApp.domain.composite.repository;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import com.twuc.webApp.domain.composite.entity.CompanyProfile;
import com.twuc.webApp.domain.composite.entity.Profile;
import com.twuc.webApp.domain.composite.entity.UserProfile;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SimpleMappingAndValueTypeTest extends JpaTestBase {

    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Test
    void should_save_get_company_profile() {
        ClosureValue<Long> companyProfileId = new ClosureValue<>();
        flushAndClear(em -> {
            CompanyProfile companyProfile = companyProfileRepository.save(
                    new CompanyProfile(new Profile("Xi'an", "tiangu")));
            companyProfileId.setValue(companyProfile.getId());
        });
        Optional<CompanyProfile> companyOptional = companyProfileRepository.findById(companyProfileId.getValue());
        assertTrue(companyOptional.isPresent());
    }

    @Test
    void should_save_get_user_profile() {

        ClosureValue<Long> userProfileId = new ClosureValue<>();
        flushAndClear(em -> {
            UserProfile userProfile = userProfileRepository.save(
                    new UserProfile(new Profile("Xi'an", "tiangu")));
            userProfileId.setValue(userProfile.getId());
        });
        Optional<UserProfile> userProfileOption = userProfileRepository.findById(userProfileId.getValue());
        assertTrue(userProfileOption.isPresent());
    }

}
